
/*
 Que 2 : WAP to print the following pattern
 Take row input from user
 1
 2 1
 3 2 1
 4 3 2 1
*/

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no.of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1 ; i <= row ; i++) {
			int x = i;
			for(int j = 1 ; j <= i ; j++) {
				System.out.print(x + " ");
				x--;
			}
			System.out.println();
		}
	}
}
