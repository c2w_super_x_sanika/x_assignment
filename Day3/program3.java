
/*
 Que 3 : WAP to check whether the given no is a palindrome number or not.
*/

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number : ");
		int num = Integer.parseInt(br.readLine());

		int temp = num;
		int rev = 0;
		while(temp != 0) {
			int rem = temp % 10;
			rev = rev * 10 + rem;
			temp = temp / 10;
		}

		if(rev == num) {
			System.out.println("The given number is Palindrome!!!");
		}else {
			
			System.out.println("The given number is not a Palindrome!!!");
		}
	}
}
