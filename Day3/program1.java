
/*
 Que 1 : WAP to print the following pattern
 Take input from user
 A B C D
 D C B A
 A B C D
 D C B A
*/

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		char ch = 'A';
		for(int i = 1 ; i <= row ; i++) {
			for(int j = 1 ; j <= row ; j++) {
				if(i % 2 != 0) {
					System.out.print(ch + " ");
					ch++;
				}else {
					System.out.print(ch + " ");
					ch--;
				}
			}
			if(i % 2 != 0) {
				ch--;
			}else {
				ch++;
			}
			System.out.println();
		}
	}
}
