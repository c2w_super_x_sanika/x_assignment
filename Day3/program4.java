
/*
 Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449
*/

class Demo {
	public static void main(String[] args) {
		int start = 25435;
		int end = 25449;

		for(int i = start ; i <= end ; i++) {
			int temp = i;
			int rev = 0;
			while(temp != 0) {
				int rem = temp % 10;
				rev = rev * 10 + rem;
				temp = temp / 10;
			}
			System.out.println("The reverse of " + i + " is " + rev);
		}
	}	
}
