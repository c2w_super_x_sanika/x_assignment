
/*
 Que 5 : WAP to check whether the string contains characters other than
 letters.
*/

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the String : ");
		String str = br.readLine();
		boolean nonLetters = nonLetter(str);

		if(nonLetters) {
			System.out.println("The string contains special characters...");
		}else {
			System.out.println("The string does not contain any special character...");
		}

	}

	static boolean nonLetter(String str) {
		char str1[] = str.toCharArray();
		for(int i = 0 ; i < str1.length ; i++) {
			if(!isLetter(str1[i])) {
				return true;
			}
		}
		return false;
	}

	static boolean isLetter(char c) {
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
	}
}
