
/*
 Que 1: WAP to print the factorial of digits in a given range.
Input: 1-10
*/

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter start : ");
		int start = Integer.parseInt(br.readLine());
		System.out.print("Enter end : ");
		int end = Integer.parseInt(br.readLine());

		for(int i = start ; i <= end ; i++) {
			int fact = 1;
			for(int j = 1 ; j <= i ; j++) {
				fact = fact * j;
			}
			System.out.println("The factorial of " + i + " : " + fact);
		}
	}
}
