
/*
 Que 3: WAP to check whether the given number is a strong number or not.
*/

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number : ");
		int num = Integer.parseInt(br.readLine());

		int temp = num;
		int sum = 0;
		while(temp != 0) {
			int rem = temp % 10;
			int fact = 1;
			for(int i = 1 ; i <= rem ; i++) {
				fact = fact * i;
			}
			sum = sum + fact;
			temp = temp / 10;
		}
		
		if(sum == num) {
			System.out.println("The given number is a strong number...");
		}else {
			System.out.println("The given number is not a strong number...");
		}
	}
}
