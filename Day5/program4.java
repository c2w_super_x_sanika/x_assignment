/*WAP to print the following pattern
Take input from the user
1  3  5  7
2  4  6  8
9  11 13 15
10 12 14 16
*/

class Pattern{
	public static void main(String[] args){
		int num = 1;
		int N = 4;
		for(int i=1;i<=N;i++){
			int flag = 0;
			for(int j=1;j<=N;j++){
				if(i%2==1){
					System.out.print(num + " ");
					num = num + 2;
					flag = 1;
				}
				else if(i%2==0){
					System.out.print(num + " ");
					num = num + 2;
				}
			}
			System.out.println();
			if(flag == 1)
				num = num-7;
			else
				num = num-1;
		}
	}
}



