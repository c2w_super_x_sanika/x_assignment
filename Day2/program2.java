
/*
1
2  4
3  6  9
4  8  12  16
*/

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no.of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 0 ; i < row ; i++) {
			int x = i + 1;
			int sum = x;
			for(int j = 0 ; j <= i ; j++) {
				System.out.print(x + " ");
				x = x + sum;
			}
			System.out.println();
		}
	}
}
