
//WAP to check if the string contains vowels and return the count of the vowels


import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the String : ");
		String str = br.readLine().toLowerCase();

		int count = 0;
		for(int i = 0 ; i < str.length() ; i++) {
			if(str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u') {
				count++;
			}
		}

		if(count == 0) {
			System.out.println("The given string does not cotain any vowels ");
		}else {
			System.out.println("The given string contains : " + count + " vowels");
		}
		
	}
}
