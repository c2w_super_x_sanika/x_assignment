
// WAP to Check whether a given number is prime or composite

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number : ");
		int num = Integer.parseInt(br.readLine());

		int count = 0;
		for(int i = 1 ; i <= num ; i++) {
			if(num == 1) {
				System.out.println(num + " neither prime nor composite");
				break;	
			}
			if(num % i == 0) {
				count++;
			}
			if(count > 2) {
				System.out.println(num + " is an composite number");
				break;
			}
		}
		if(count == 2) {
			System.out.println(num + " is a prime number ");
		}
	}
}
