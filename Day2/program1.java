
/*
A B C D
B C D E
C D E F
D E F G
*/

import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no.of rows : ");
		int row = Integer.parseInt(br.readLine());

		char ch = 'A';
		for(int i = 1 ; i <= row ; i++) {
			char temp = ch;
			for(int j = 1 ; j <= row ; j++) {
				System.out.print(temp + " ");
				temp++;
			}
			ch++;
			System.out.println();

		}
	}
}
