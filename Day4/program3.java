///WAP to find the factorial of a given number.

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number:");
		int num = Integer.parseInt(br.readLine());
		int fact = 1;
		for(int i=1;i<=num;i++){
			fact = fact * i;
		}
		System.out.println(fact);
	}
}

