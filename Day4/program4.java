/*4.WAP to print the sum of digits in given range.
 * Input 1 to 10
 * Input 21 to 30
 */


import java.io.*;

class SumOfDigit {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter range : ");

		int start = Integer.parseInt(br.readLine());
		int end = Integer.parseInt(br.readLine());
		
		int sum = 0;

		for(int i=start;i<=end;i++) {

			sum = sum + i;

		}

		System.out.println("Sum = "+sum);
	}
}

