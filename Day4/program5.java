/*5.WAP to toggle the String to uppercase or lowercase.
 * Input 1 : Java
 * Output 1 : jAVA
 *
 * Input 2 : data
 * Output 2 :DATA
 */

import java.io.*;

class StringDemo {

	static String toggleString(String str) {

		char arr[] = str.toCharArray();

		for(int i=0;i<str.length();i++) {

			if(arr[i] >= 'A' && arr[i] <= 'Z'){
		
				arr[i] =(char)(arr[i] + 32);
		
			}else if(arr[i] >= 'a' && arr[i] <= 'z'){

				arr[i] =(char)(arr[i] - 32);
			}
		}

		return new String(arr);
	}

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String : ");

		String str = br.readLine();

		System.out.println(toggleString(str));
	}
}
