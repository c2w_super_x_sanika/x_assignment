/*WAP to print the following pattern
Take input from the user
A B C D
# # # #
A B C D
# # # #
A B C D
*/
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of Rows:");
		int row = Integer.parseInt(br.readLine());

		System.out.println("Enter character:");
		char ch1 = (char)br.read();
		br.skip(1);
	       	System.out.println("Enter System:");
		char ch2 = (char)br.read();	
	
		for(int i=0;i<row;i++){
			int flag = 0;
			for(int j=0;j<row;j++){

				if(i % 2== 0){
					System.out.print(ch1 + " ");
					ch1++;
					flag = 1;
				}else if(i%2 ==1){
					System.out.print(ch2 + " ");
				}
			}
			if(flag == 1)
				ch1 = (char)(ch1-4);
			System.out.println();
		}
	}
}

