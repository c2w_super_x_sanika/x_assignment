/*WAP to print the following pattern
Take row input from the user
A
B A
C B A
D C B A
*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of Rows:");
		int row = Integer.parseInt(br.readLine());

		char ch1 = 'A';

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(ch1 + " ");
				ch1--;
			}
			ch1 = (char)(ch1+i+1);
			System.out.println();
		}
	}
}

