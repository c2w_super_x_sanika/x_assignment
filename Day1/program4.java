//4
//WAP to print the odd numbers in the given range 
//I/p: start - 1, end - 10

class Number {

	public static void main(String[] args) {

		for(int i=1;i<=10;i++) {

			if(i%2 != 0) {

				System.out.print(i + "  ");
			}
		}
	}
}


