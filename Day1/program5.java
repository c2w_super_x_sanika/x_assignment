
//5.WAP to count the size of given string without using in-built function.
 
import java.io.*;

class StringDemo {

	static int countSize(String str) {

		char arr[] = str.toCharArray();

		int count = 0;

		for(int i=0;i<arr.length;i++) {

			count++;

		}

		return count;
	}

	public static void main(String[] args) throws IOException{

		System.out.println("Enter String : ");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = br.readLine();

		System.out.println("Count = "+countSize(str));
	}
}
